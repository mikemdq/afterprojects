using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateWeapon : MonoBehaviour
{
    public ChooseWeapon pickWeapon;
    public int numberOfWeapon;
    // Start is called before the first frame update
    void Start()
    {
        pickWeapon = GameObject.FindGameObjectWithTag("Player").GetComponent<ChooseWeapon>();
    }

    // Update is called once per frame
    public void OntriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            pickWeapon.EquipWeapond(numberOfWeapon);
            Destroy(gameObject);
        }
    }
}
