using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RPickUpWeapon : MonoBehaviour
{
    public GameObject rWeaponToPick;
    public Transform rHand;
    public AudioSource pickSFX;
    public PlayerContoller handOccupied;
    void Start()
    {
        
    }
    void Update()
    {
        
    }
    public void  OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            handOccupied.rHandOccupied = true;
            Instantiate(rWeaponToPick, rHand);
            pickSFX.Play();
            Destroy(gameObject);
        }
    }
}
