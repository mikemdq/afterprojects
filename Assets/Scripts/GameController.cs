using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [Header("UI")]
    public GameObject createCharacterUI;
    [Header("Player")]
    public PlayerContoller player;
    private bool isPaused = false;
    [Header("Sound FX")]
    public AudioSource openMenu;
    public AudioSource closeMenu;
    void Start()
    {
        
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            isPaused = !isPaused;
            openMenu.Play();

        }

        if (isPaused == true)
        {
            player.enabled =false;
            createCharacterUI.SetActive(true);
            
        }
        else
        {
            player.enabled = true;
            createCharacterUI.SetActive(false);
        }
    }
}
