using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseWeapon : MonoBehaviour
{
    [Header("Weaponds")]
    public GameObject [] weaponds;
    

    [Header("Hands")]
    public GameObject handL;
    public GameObject handR;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EquipWeapond(int order)
    {
        for (int w = 0; w < weaponds.Length; w++)
        {
            weaponds[w].SetActive(false);
        }
        weaponds[order].SetActive(true);
    }
}
