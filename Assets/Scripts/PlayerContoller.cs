using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerContoller : MonoBehaviour
{
    float hMove;
    float vMove;
    Vector3 _input;
    Animator anim;
    [Header("Player")]
    public CharacterController player;
    public Transform playerCamera;
    [Header ("Hand Occupied")]
    public bool lHandOccupied = false;
    public bool rHandOccupied = false;
    [Header("Movement")]
    public float speed;
    public float turnTime = 0.1f;
    float turnSpeed;
    [Header("Hold Weapond")]
    public Transform strap;
    [Header("Life Controller")]
    public int Life = 50;
    [Header("Sounds")]
    public AudioSource takeDamage;


    void Start()
    {
        player = GetComponent<CharacterController>(); //toma el componente al empezar la escena
        anim = GetComponent<Animator>();


    }
    void Update()
    {
        MovementPlayer();
        HoldWeapond();
        if (rHandOccupied == true){AttackPlayer();}
        if (lHandOccupied == true){GuardPlayer();}
        

        
        
    }
    void MovementPlayer()
    {
        hMove = Input.GetAxisRaw("Horizontal");
        vMove = Input.GetAxisRaw("Vertical");
        _input = new Vector3(hMove, 0, vMove).normalized;
        _input = Vector3.ClampMagnitude(_input, 1);
        if (_input.magnitude >= 0.1f)
        {
            float targetAngle = Mathf.Atan2(_input.x, _input.z) * Mathf.Rad2Deg + playerCamera.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSpeed, turnTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);
            Vector3 movement = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            player.Move(movement.normalized * speed * Time.deltaTime);
        }
        
    }
    void AttackPlayer() 
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            anim.SetTrigger("attack");
        }
    }
    void GuardPlayer()
    {
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            anim.SetBool ("guard", true);
        }
        else
        {
            anim.SetBool("guard", false);
        }

    }
    void HoldWeapond()
    {
        if (rHandOccupied == true)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                rHandOccupied = false;
            }
        }
        if (lHandOccupied == true)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                lHandOccupied = false;
            }
        }



    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Damage"))
        {
            Life --;
            anim.SetTrigger("takedamage");
            takeDamage.Play();
        }
    }


}
