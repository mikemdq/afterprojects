using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangePlayer : MonoBehaviour
{
    [Header("hair")]
    public GameObject hair1;
    public GameObject hair2;
    public GameObject hair3;
    public GameObject hair4;
    public GameObject hair5;
    public GameObject hair6;
    public GameObject hair7;
    public GameObject hair8;
    public GameObject hairnone;
    [Header("eyebrow")]
    public GameObject eyebrow1;
    public GameObject eyebrow2;
    public GameObject eyebrow3;
    public GameObject eyebrow4;
    [Header("facialhair")]
    public GameObject facialhair1;
    public GameObject facialhair2;
    public GameObject facialhair3;
    public GameObject facialhair4;
    public GameObject facialhairnone;
    [Header("skin")]
    public GameObject body;
    public Material asian;
    public Material caucacian;
    public Material dark;
    [Header("colorhair")]
    public Material black;
    public Material blonde;
    public Material red;
    public Material white;
    public void Hair(int value)
    {
        if (value == 0) 
        {
            hair1.SetActive(true);
            hair2.SetActive(false);
            hair3.SetActive(false);
            hair4.SetActive(false);
            hair5.SetActive(false);
            hair6.SetActive(false);
            hair7.SetActive(false);
            //hair8.SetActive(false);
            hairnone.SetActive(false);
        }
        if (value == 1)
        {
            hair1.SetActive(false);
            hair2.SetActive(true);
            hair3.SetActive(false);
            hair4.SetActive(false);
            hair5.SetActive(false);
            hair6.SetActive(false);
            hair7.SetActive(false);
            //hair8.SetActive(false);
            hairnone.SetActive(false);
        }
        if (value == 2)
        {
            hair1.SetActive(false);
            hair2.SetActive(false);
            hair3.SetActive(true);
            hair4.SetActive(false);
            hair5.SetActive(false);
            hair6.SetActive(false);
            hair7.SetActive(false);
            //hair8.SetActive(false);
            hairnone.SetActive(false);
        }
        if (value == 3)
        {
            hair1.SetActive(false);
            hair2.SetActive(false);
            hair3.SetActive(false);
            hair4.SetActive(true);
            hair5.SetActive(false);
            hair6.SetActive(false);
            hair7.SetActive(false);
            //hair8.SetActive(false);
            hairnone.SetActive(false);
        }
        if (value == 4)
        {
            hair1.SetActive(false);
            hair2.SetActive(false);
            hair3.SetActive(false);
            hair4.SetActive(false);
            hair5.SetActive(true);
            hair6.SetActive(false);
            hair7.SetActive(false);
            //hair8.SetActive(false);
            hairnone.SetActive(false);
        }
        if (value == 5)
        {
            hair1.SetActive(false);
            hair2.SetActive(false);
            hair3.SetActive(false);
            hair4.SetActive(false);
            hair5.SetActive(false);
            hair6.SetActive(true);
            hair7.SetActive(false);
            //hair8.SetActive(false);
            hairnone.SetActive(false);
        }
        if (value == 6)
        {
            hair1.SetActive(false);
            hair2.SetActive(false);
            hair3.SetActive(false);
            hair4.SetActive(false);
            hair5.SetActive(false);
            hair6.SetActive(false);
            hair7.SetActive(true);
            //hair8.SetActive(false);
            hairnone.SetActive(false);
        }
        if (value == 7)
        {
            hair1.SetActive(false);
            hair2.SetActive(false);
            hair3.SetActive(false);
            hair4.SetActive(false);
            hair5.SetActive(false);
            hair6.SetActive(false);
            hair7.SetActive(false);
            //hair8.SetActive(true);
            hairnone.SetActive(false);
        }
        if (value == 8)
        {
            hair1.SetActive(false);
            hair2.SetActive(false);
            hair3.SetActive(false);
            hair4.SetActive(false);
            hair5.SetActive(false);
            hair6.SetActive(false);
            hair7.SetActive(false);
            //hair8.SetActive(false);
            hairnone.SetActive(true);
        }



    }
    public void Eyebrow(int value)
    {
        if (value == 0)
        {
            eyebrow1.SetActive(true);
            eyebrow2.SetActive(false);
            eyebrow3.SetActive(false);
            eyebrow4.SetActive(false);
        }
        if (value == 1)
        {
            eyebrow1.SetActive(false);
            eyebrow2.SetActive(true);
            eyebrow3.SetActive(false);
            eyebrow4.SetActive(false);
        }
        if (value == 2)
        {
            eyebrow1.SetActive(false);
            eyebrow2.SetActive(false);
            eyebrow3.SetActive(true);
            eyebrow4.SetActive(false);
        }
        if (value == 3)
        {
            eyebrow1.SetActive(false);
            eyebrow2.SetActive(false);
            eyebrow3.SetActive(false);
            eyebrow4.SetActive(true);
        }
    }
    public void FacialHair(int value) 
    {
        if (value == 0)
        {
            facialhair1.SetActive(true);
            facialhair2.SetActive(false);
            facialhair3.SetActive(false);
            facialhair4.SetActive(false);
            facialhairnone.SetActive(false);
        }
        if (value == 1)
        {
            facialhair1.SetActive(false);
            facialhair2.SetActive(true);
            facialhair3.SetActive(false);
            facialhair4.SetActive(false);
            facialhairnone.SetActive(false);
        }
        if (value == 2)
        {
            facialhair1.SetActive(false);
            facialhair2.SetActive(false);
            facialhair3.SetActive(true);
            facialhair4.SetActive(false);
            facialhairnone.SetActive(false);
        }
        if (value == 3)
        {
            facialhair1.SetActive(false);
            facialhair2.SetActive(false);
            facialhair3.SetActive(false);
            facialhair4.SetActive(true);
            facialhairnone.SetActive(false);
        }
        if (value == 4)
        {
            facialhair1.SetActive(false);
            facialhair2.SetActive(false);
            facialhair3.SetActive(false);
            facialhair4.SetActive(false);
            facialhairnone.SetActive(true);
        }


    }
    public void Skin(int value) 
    {
        if (value == 0)
        {
            body.GetComponent<MeshRenderer>().material = asian;
           
        }
        if (value == 1)
        {
            body.GetComponent<MeshRenderer>().material = caucacian;
           
        }
        if (value == 2)
        {
            body.GetComponent<MeshRenderer>().material = dark;
            
        }
    }
    public void ColorHair(int value) 
    {
        if (value == 0)
        {
            hair1.GetComponent<MeshRenderer>().material = black;
            hair2.GetComponent<MeshRenderer>().material = black;
            hair3.GetComponent<MeshRenderer>().material = black;
            hair4.GetComponent<MeshRenderer>().material = black;
            hair5.GetComponent<MeshRenderer>().material = black;
            hair6.GetComponent<MeshRenderer>().material = black;
            hair7.GetComponent<MeshRenderer>().material = black;
            hair8.GetComponent<MeshRenderer>().material = black;
            eyebrow1.GetComponent<MeshRenderer>().material = black;
            eyebrow2.GetComponent<MeshRenderer>().material = black;
            eyebrow3.GetComponent<MeshRenderer>().material = black;
            eyebrow4.GetComponent<MeshRenderer>().material = black;
            facialhair1.GetComponent<MeshRenderer>().material = black;
            facialhair2.GetComponent<MeshRenderer>().material = black;
            facialhair3.GetComponent<MeshRenderer>().material = black;
            facialhair4.GetComponent<MeshRenderer>().material = black;
        }
        if (value == 1)
        {
            hair1.GetComponent<MeshRenderer>().material = red;
            hair2.GetComponent<MeshRenderer>().material = red;
            hair3.GetComponent<MeshRenderer>().material = red;
            hair4.GetComponent<MeshRenderer>().material = red;
            hair5.GetComponent<MeshRenderer>().material = red;
            hair6.GetComponent<MeshRenderer>().material = red;
            hair7.GetComponent<MeshRenderer>().material = red;
            hair8.GetComponent<MeshRenderer>().material = red;
            eyebrow1.GetComponent<MeshRenderer>().material = red;
            eyebrow2.GetComponent<MeshRenderer>().material = red;
            eyebrow3.GetComponent<MeshRenderer>().material = red;
            eyebrow4.GetComponent<MeshRenderer>().material = red;
            facialhair1.GetComponent<MeshRenderer>().material = red;
            facialhair2.GetComponent<MeshRenderer>().material = red;
            facialhair3.GetComponent<MeshRenderer>().material = red;
            facialhair4.GetComponent<MeshRenderer>().material = red;
        }
        if (value == 2)
        {
            hair1.GetComponent<MeshRenderer>().material = blonde;
            hair2.GetComponent<MeshRenderer>().material = blonde;
            hair3.GetComponent<MeshRenderer>().material = blonde;
            hair4.GetComponent<MeshRenderer>().material = blonde;
            hair5.GetComponent<MeshRenderer>().material = blonde;
            hair6.GetComponent<MeshRenderer>().material = blonde;
            hair7.GetComponent<MeshRenderer>().material = blonde;
            hair8.GetComponent<MeshRenderer>().material = blonde;
            eyebrow1.GetComponent<MeshRenderer>().material = blonde;
            eyebrow2.GetComponent<MeshRenderer>().material = blonde;
            eyebrow3.GetComponent<MeshRenderer>().material = blonde;
            eyebrow4.GetComponent<MeshRenderer>().material = blonde;
            facialhair1.GetComponent<MeshRenderer>().material = blonde;
            facialhair2.GetComponent<MeshRenderer>().material = blonde;
            facialhair3.GetComponent<MeshRenderer>().material = blonde;
            facialhair4.GetComponent<MeshRenderer>().material = blonde;
        }
        if (value == 3)
        {
            hair1.GetComponent<MeshRenderer>().material = white;
            hair2.GetComponent<MeshRenderer>().material = white;
            hair3.GetComponent<MeshRenderer>().material = white;
            hair4.GetComponent<MeshRenderer>().material = white;
            hair5.GetComponent<MeshRenderer>().material = white;
            hair6.GetComponent<MeshRenderer>().material = white;
            hair7.GetComponent<MeshRenderer>().material = white;
            hair8.GetComponent<MeshRenderer>().material = white;
            eyebrow1.GetComponent<MeshRenderer>().material = white;
            eyebrow2.GetComponent<MeshRenderer>().material = white;
            eyebrow3.GetComponent<MeshRenderer>().material = white;
            eyebrow4.GetComponent<MeshRenderer>().material = white;
            facialhair1.GetComponent<MeshRenderer>().material = white;
            facialhair2.GetComponent<MeshRenderer>().material = white;
            facialhair3.GetComponent<MeshRenderer>().material = white;
            facialhair4.GetComponent<MeshRenderer>().material = white;
        }



    }
}
