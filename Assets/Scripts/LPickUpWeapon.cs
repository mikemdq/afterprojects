using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LPickUpWeapon : MonoBehaviour
{
    public GameObject lWeaponToPick;
    public Transform lHand;
    public AudioSource pickSFX;
    public PlayerContoller handOccupied;
    void Start()
    {
        
    }
    void Update()
    {
        
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            handOccupied.lHandOccupied = true;
            Instantiate(lWeaponToPick, lHand);
            pickSFX.Play();
            Destroy(gameObject);
        }
    }
}
