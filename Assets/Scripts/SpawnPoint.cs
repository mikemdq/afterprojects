using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    public GameObject objectToSpawn;
    public float timeToStart = 10f;
    public float repeatTime = 6f;

    private void Start()
    {
        InvokeRepeating("Spawn", timeToStart, repeatTime);
    }
    void Spawn()
    {
        //Este cuaterni�n corresponde a "sin rotaci�n": el objeto est� identicamente alineado con el mundo o los ejes principales.
        Instantiate(objectToSpawn, transform.position, Quaternion.identity);
    }
}
