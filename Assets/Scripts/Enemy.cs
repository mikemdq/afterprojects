using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed;
    Transform target;
    Animator anim;
    MeshRenderer meshRenderer;
    Color originalColor;
    float flashTime = 0.10f;
    float distanceToTarget;
    //---------------------------//
    [Header("Weapon")]
    public GameObject weapon;
    [Header("Life")]
    public int enemyLife = 10;
    [Header("Sounds")]
    public AudioSource enemyDamage;
    public GameObject deadSFX;
    

    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        anim = GetComponent<Animator>();
        meshRenderer = GetComponent<MeshRenderer>();
        originalColor = meshRenderer.material.color;

    }
    void Update()
    {
        if (enemyLife >= 1) { Movement(); Attack(); }
        if (enemyLife <= 0) { Dead();}
        
    }
    void Movement()
    {
        transform.LookAt(target);
        transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        distanceToTarget = Vector3.Distance(target.position, transform.position);
    }
    void Attack()
    {
        if (distanceToTarget <= 5f)
        {
            anim.SetBool("attack", true);
        }
        if (distanceToTarget >= 5f)
        {
            anim.SetBool("attack", false);
            anim.SetTrigger("Idle");
        }
    }
    private void OnTriggerEnter(Collider damage)
    {
        if (enemyLife >= 1)
        {
            if (damage.CompareTag("takeDamage"))
            {
                enemyLife--;
                enemyDamage.Play();
                anim.SetTrigger("surprised");
                StartCoroutine(EFlash());
            }
        }

        }
    IEnumerator EFlash()
    {
        meshRenderer.material.color = Color.red;
        yield return new WaitForSeconds(flashTime);
        meshRenderer.material.color = originalColor;
    }
    void Dead()
    {
        Destroy(weapon);
        deadSFX.SetActive(true);
        meshRenderer.material.color = Color.gray;
    }

}
